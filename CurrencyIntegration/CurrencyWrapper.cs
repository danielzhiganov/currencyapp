﻿using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace CurrencyIntegration
{
    public class CurrencyWrapper : ICurrencyWrapper
    {
        private readonly string _currencyJsonUrl = "https://www.cbr-xml-daily.ru/daily_json.js";
        public async Task<string[]> GetAllCurrencies()
        {
            string currencyJson;
            string[] currencies;
            string currencyPattern = '"' + "charcode" + '"' + ": " + '"' + "(?<currencyCode>.*?)" + '"';
            using (WebClient wc = new WebClient())
            {
                currencyJson = (await wc.DownloadStringTaskAsync(_currencyJsonUrl)).ToLower();
                MatchCollection matches = Regex.Matches(currencyJson, currencyPattern);
                currencies = matches.Select(x => x.Groups[1].Value).ToArray();
            }
            return currencies;
        }

        public async Task<float> GetValueByCurrency(string currency)
        {
            float valueByCurrency;
            currency = currency.ToLower();
            string currencyJson;
            string currencyPattern = '"' + "value" + '"' + ": " + "(?<currency>(?<currency>[-.0-9]+))";
            using (WebClient wc = new WebClient())
            {
                currencyJson = (await wc.DownloadStringTaskAsync(_currencyJsonUrl)).ToLower();
                var jsonObjects = (JObject)JsonConvert.DeserializeObject(currencyJson);
                try
                {
                    string neededCurrency = jsonObjects.Last.First.Where(x => x.ToString().Contains(currency)).FirstOrDefault().ToString();
                    MatchCollection matches = Regex.Matches(currencyJson, currencyPattern);
                    string stringValue = matches.Select(x => x.Groups[1].Value).FirstOrDefault();
                    valueByCurrency = float.Parse(stringValue);
                }
                catch
                {

                    throw new NullReferenceException("Please enter valid currency");
                }
            }
            return valueByCurrency;
        }
    }
}
