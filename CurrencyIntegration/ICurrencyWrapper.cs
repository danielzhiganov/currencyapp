﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyIntegration
{
    public interface ICurrencyWrapper
    {
        Task<string[]> GetAllCurrencies();
        Task<float> GetValueByCurrency(string currency);
    }
}
