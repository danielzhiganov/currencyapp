﻿using CurrencyIntegration;
using Services.Contracts;
using System;
using System.Threading.Tasks;

namespace Services
{
    public class CurrencyService : ICurrencyService
    {
        private ICurrencyWrapper _currencyWrapper;
        public CurrencyService(ICurrencyWrapper currencyWrapper)
        {
            _currencyWrapper = currencyWrapper;
        }
        public async Task<string[]> GetAllCurrencies()
        {
            string[] currencies = await _currencyWrapper.GetAllCurrencies();
            return currencies;
        }

        public async Task<float> GetValueByCurrency(string currency)
        {
            float currencyValue = await _currencyWrapper.GetValueByCurrency(currency);
            return currencyValue;
        }
    }
}
