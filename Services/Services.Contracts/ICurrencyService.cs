﻿using System;
using System.Threading.Tasks;

namespace Services.Contracts
{
    public interface ICurrencyService
    {
        Task<string[]> GetAllCurrencies();
        Task<float> GetValueByCurrency(string currency);
    }
}
