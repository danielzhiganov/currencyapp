﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyApp.Controllers
{
    [ApiController]
    [Route("api/currency")]
    public class CurrencyController : ControllerBase
    {
        private readonly ILogger<CurrencyController> _logger;
        private ICurrencyService _currencyService;
        public CurrencyController(ILogger<CurrencyController> logger, ICurrencyService currencyService)
        {
            _currencyService = currencyService;
            _logger = logger;
        }

        [HttpGet]
        [Route("all")]
        public async Task<ActionResult> GetAllCurrencies()
        {
            var currencies = await _currencyService.GetAllCurrencies();
            return Ok(currencies);
        }

        [HttpGet]
        [Route("{currency}")]
        public async Task<ActionResult> GetValueByCurrency(string currency)
        {
            var currencyValue = await _currencyService.GetValueByCurrency(currency);
            return Ok(currencyValue);
        }
    }
}
